FROM nginx:alpine

RUN \
    awk '!x{x=sub("/usr/share/nginx/html","/data")}1' /etc/nginx/conf.d/default.conf > /tmp/d && mv /tmp/d /etc/nginx/conf.d/default.conf && \
    sed -i 's#location \/ {#location \/ { autoindex on;#g' /etc/nginx/conf.d/default.conf && \
    mkdir /data && \
    echo 'echo "Generating files"; for i in 1 10 100 1000; do dd if=/dev/urandom of=/data/${i}MB.bin bs=1M count=${i}; done && echo "Done" && exec nginx -g "daemon off;"' > /startup && \
    chmod +x /startup && \
    :

HEALTHCHECK CMD wget -q --spider http://localhost/1000MB.bin

CMD /startup
